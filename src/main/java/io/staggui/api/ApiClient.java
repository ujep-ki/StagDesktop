package io.staggui.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.staggui.api.exceptions.NonValidTokenException;
import io.staggui.api.models.*;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import lombok.Getter;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

/**
 * Singleton class for making requests against stag server api
 */
public class ApiClient {
    //Error response code when WS token is not valid
    private static final int UNAUTHORIZED_STATUS = 401;
    private static final int SUCCESS_STATUS = 200;


    //Singleton object for this class instance
    private static ApiClient INSTANCE;

    // jackson: serializing (java object To JSON) and deserializing (vice versa)
    private final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Retrieves singleton class
     * @return ApiClient singleton class
     */
    public static ApiClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ApiClient();
        }

        return INSTANCE;
    }

    private String baseUrl;
    private String wsCookie;
    @Getter
    private StudentInfo student;
    private String id;
    @Getter
    private List<UserRole> roles;

    private final HttpClient httpClient;

    private ApiClient() {
        //initiate default base url
        baseUrl = "https://ws.ujep.cz/ws/services/rest2";
        httpClient = HttpClient.newHttpClient();
    }

    /**
     * Function that premake HTTPRequst with default params
     * @param url sting parameter that defines full or partial uri of request
     * @return HttpRequest with default params
     */
    private HttpRequest createRequest(String url) {
        //warns user that has defined baseurl and uses it in request again
        if(baseUrl != null && url.startsWith(baseUrl)) {
            System.err.println("Warning! Url already contains 'baseUrl'");
        }

        return HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(baseUrl != null ? baseUrl + url : url))
                //adding authorizing header to request
                .header("Cookie", "WSCOOKIE="+wsCookie)
                //adding response type header (telling the server to send response of this type)
                .header("Accept", "application/json")
                .build();
    }

    /**
     * Retrieves task for requesting timetable
     * @param term term which you want quarry
     * @param year year which you want quarry
     * @return Task for requesting TimeTable
     */
    public Task<Optional<TimeTable>> createTimeTableTask(Term term, Integer year) {
        //creates and returns background task template
        return new Task<>() {
            @Override
            protected Optional<TimeTable> call() throws Exception {
                //build url with GET parameters
                String url = URLBuilder
                    .create("/rozvrhy/getRozvrhByStudent")
                    .add("stagUser", id)
                    .add("osCislo", id)
                    .add("rok", year)
                    .add("semestr", term.name())
                    .build();


                //creates and makes request to server and retrieve response
                var response = httpClient.send(createRequest(url), HttpResponse.BodyHandlers.ofString());

                //checks if valid token was supplied
                if (response.statusCode() == UNAUTHORIZED_STATUS) {
                    //if token is rejected the it will fail and send info about it
                    setException(new NonValidTokenException("Token is not valid"));
                    cancel();

                    return Optional.empty();
                } else {
                    // extract string body from response and try to convert string JSON to TimeTable class and finally return it as optional object
                    return Optional.of(objectMapper.readValue(response.body(), TimeTable.class));
                }
            }
        };
    }

    /**
     * Retrieves task for requesting marks
     * @param term term which you want quarry
     * @param year year which you want quarry
     * @return Task for requesting Marks
     */
    public Task<List<Mark>> createMarksTask(Term term, Integer year) {
        //creates and returns background task template
        return new Task<>() {
            @Override
            protected List<Mark> call() throws Exception {
                String url = URLBuilder
                    .create("/znamky/getZnamkyByStudent")
                    .add("stagUser", id)
                    .add("osCislo", id)
                    .add("rok", year)
                    .add("semestr", term.name())
                    .build();

                var response = httpClient.send(createRequest(url), HttpResponse.BodyHandlers.ofString());

                if (response.statusCode() == UNAUTHORIZED_STATUS) {
                    setException(new NonValidTokenException("Token is not valid"));
                    cancel();

                    return new ArrayList<>();
                } else {
                    // extract string body from response and try to convert string JSON to Marks class and finally return array of non empty list marks
                    return objectMapper.readValue(response.body(), Marks.class).getStudentNaPredmetu();
                }
            }
        };
    }

    /**
     * Task for requesting CurrentAcademicStateInfo
     * @return Task that will process request of CurrentAcademicStateInfo data
     */
    public Task<Optional<CurrentAcademicStateInfo>> createCurrentAcademicInfoTask() {
        //creates and returns background task template
        return new Task<>() {
            @Override
            protected Optional<CurrentAcademicStateInfo> call() throws Exception {
                String url = "/kalendar/getAktualniObdobiInfo";

                var response = httpClient.send(createRequest(url), HttpResponse.BodyHandlers.ofString());

                if (response.statusCode() == UNAUTHORIZED_STATUS) {
                    setException(new NonValidTokenException("Token is not valid"));
                    cancel();

                    return Optional.empty();
                } else {
                    // extract string body from response and try to convert string JSON to CurrentAcademicStateInfo class and finally return it as optional object
                    return Optional.of(objectMapper.readValue(response.body(), CurrentAcademicStateInfo.class));
                }
            }
        };
    }

    /**
     * Function that parse and stores needed data for requests
     * @param userToken WSCOOKIE that is used for authentication
     * @param base64Data base64 data of logged user
     */
    public void setCredentials(String userToken, String base64Data) {
        var clearBase64Data = clearAndFix(base64Data);

        try {
            //decode base64 token (base64 - encode any data into ascii text)
            UserLoginData userLoginData = objectMapper.readValue(Base64.getDecoder().decode(clearBase64Data), UserLoginData.class);

            if(userLoginData.getStagUserInfo().isEmpty()) {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setContentText("Musíte se přihlásit aby jste mohly používat tuto aplikaci");
                alert.showAndWait();
            } else {
                this.roles = userLoginData.getStagUserInfo();
                // storing the web socket cookie (using for authentication of the session)
                this.wsCookie = userToken;
                this.id = roles.get(0).getUserName();
                requestStudentData();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void requestStudentData() {
        String url = URLBuilder
                .create("/student/getStudentInfo")
                .add("stagUser", id)
                .add("osCislo", id)
                .build();

        try {
            var response = httpClient.send(createRequest(url), HttpResponse.BodyHandlers.ofString());

            if (response.statusCode() != UNAUTHORIZED_STATUS) {
                String jsonData = response.body();

                student = objectMapper.readValue(jsonData, StudentInfo.class);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cleans end of base64 string
     * @param base64Data base64 string
     * @return clear base64 string
     */
    private String clearAndFix(String base64Data) {
        StringBuilder stringBuilder = new StringBuilder();
        char[] base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

        for (char c : base64Data.toCharArray()) {
            var exists = false;

            for (char base64Char : base64Chars) {
                if (c == base64Char) {
                    exists = true;
                    break;
                }
            }

            if(!exists) {
                return stringBuilder.toString();
            }

            stringBuilder.append(c);
        }

        return stringBuilder.toString();
    }

    /**
     * Return all authentication information
     * @return AuthDetails
     */
    public AuthDetails getDetails() {
        return new AuthDetails(baseUrl, wsCookie, id);
    }

    /**
     * Loads all data from AuthDetails class and stores them
     */
    public void loadDetails(AuthDetails authDetails) {
        this.baseUrl = authDetails.getBaseUrl();
        this.wsCookie = authDetails.getWsCookie();
        this.id = authDetails.getId();

        requestStudentData();
    }
}
