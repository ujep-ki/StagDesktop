package io.staggui.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthDetails {
    private String baseUrl;
    private String wsCookie;
    private String id;
}
