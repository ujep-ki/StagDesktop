package io.staggui.api.models;

import lombok.Data;

@Data
class PosledniVyucovaciDenRoku{
    private String value;
}

@Data
class PosledniDenSemestruInteligentne{
    private String value;
}

@Data
class PosledniDenZimnihoZkouskoveho{
    private String value;
}

@Data
class PosledniDenLetnihoZkouskoveho{
    private String value;
}

@Data
class PrvniDenStavajicihoAkademickehoRoku{
    private String value;
}

@Data
class PosledniDenStavajicihoAkademickehoRoku{
    private String value;
}

@Data
public class CurrentAcademicStateInfo {
    private Term obdobi;
    private String akademRok;
    private String semestrInteligentne;
    private String akademRokInteligentne;
    private PosledniVyucovaciDenRoku posledniVyucovaciDenRoku;
    private PosledniDenSemestruInteligentne posledniDenSemestruInteligentne;
    private PosledniDenZimnihoZkouskoveho posledniDenZimnihoZkouskoveho;
    private PosledniDenLetnihoZkouskoveho posledniDenLetnihoZkouskoveho;
    private PrvniDenStavajicihoAkademickehoRoku prvniDenStavajicihoAkademickehoRoku;
    private PosledniDenStavajicihoAkademickehoRoku posledniDenStavajicihoAkademickehoRoku;
}
