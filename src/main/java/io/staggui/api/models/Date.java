package io.staggui.api.models;

import lombok.Data;

@Data
public class Date {
    private String value;
}
