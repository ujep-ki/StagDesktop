package io.staggui.api.models;

import lombok.Data;

@Data
public class DateFrom {
    private String value;
}
