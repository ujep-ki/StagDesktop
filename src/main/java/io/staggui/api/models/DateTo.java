package io.staggui.api.models;

import lombok.Data;

@Data
public class DateTo {
    private String value;
}
