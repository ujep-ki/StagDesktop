package io.staggui.api.models;

import com.fasterxml.jackson.annotation.JsonValue;

public enum DayEnum {
    Mon("Po"), Tue("Út"), Wed("St"), Thu("Čt"), Fri("Pá"), Sat("So"), Sun("Ne");

    public static Integer length = DayEnum.values().length;
    
    private final String name;

    DayEnum(String name) {
        this.name = name;
    }

    public static int[] ordinals() {
        DayEnum[] values = values();
        int[] ordinals = new int[values.length];

        for (int i = 0; i < ordinals.length; i++) {
            ordinals[i] = values[i].ordinal();
        }

        return ordinals;
    }

    @JsonValue
    @Override
    public String toString() {
        return name;
    }
}
