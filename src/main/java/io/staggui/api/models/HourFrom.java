package io.staggui.api.models;

import lombok.Data;

@Data
public class HourFrom {
    private String value;
}
