package io.staggui.api.models;

import lombok.Data;

@Data
public class HourTo {
    private String value;
}
