package io.staggui.api.models;

import lombok.Data;

@Data
public class StudentInfo {
    private String osCislo;
    private String jmeno;
    private String prijmeni;
    private String titulPred;
    private String titulZa;
    private String stav;
    private String userName;
    private String stprIdno;
    private String nazevSp;
    private String fakultaSp;
    private String kodSp;
    private String formaSp;
    private String typSp;
    private String typSpKey;
    private String mistoVyuky;
    private String rocnik;
    private String financovani;
    private String oborKomb;
    private String oborIdnos;
    private String email;
    private String maxDobaDatum;
    private String simsP58;
    private String simsP59;
    private String cisloKarty;
    private String pohlavi;
    private String rozvrhovyKrouzek;
    private String studijniKruh;
    private String evidovanBankovniUcet;
}
