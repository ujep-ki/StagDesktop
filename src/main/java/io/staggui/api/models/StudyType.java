package io.staggui.api.models;

public enum StudyType {
    FULLTIME, PARTTIME
}
