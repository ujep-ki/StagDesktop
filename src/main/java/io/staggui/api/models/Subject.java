package io.staggui.api.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.geometry.Rectangle2D;
import lombok.Data;

@Data
public class Subject {
    private int roakIdno;
    private String nazev;
    private String katedra;
    private String predmet;
    private String statut;
    private int ucitIdno;
    private Teacher ucitel;
    private String rok;
    private String budova;
    private String mistnost;
    private int kapacitaMistnosti;
    private int planObsazeni;
    private int obsazeni;
    private String typAkce;
    private String typAkceZkr;
    private String semestr;
    private String platnost;
    private String den;
    private DayEnum denZkr;
    private Object vyucJazyk;
    private int hodinaOd;
    private int hodinaDo;
    private int pocetVyucHodin;
    private HourFrom hodinaSkutOd;
    private HourTo hodinaSkutDo;
    private int tydenOd;
    private int tydenDo;
    private String tyden;
    private String tydenZkr;
    private int grupIdno;
    private String jeNadrazena;
    private String maNadrazenou;
    private String kontakt;
    private String krouzky;
    private String casovaRada;
    private Date datum;
    private DateFrom datumOd;
    private DateTo datumDo;
    private String druhAkce;
    private String vsichniUciteleUcitIdno;
    private String vsichniUciteleJmenaTituly;
    private String vsichniUciteleJmenaTitulySPodily;
    private String vsichniUcitelePrijmeni;
    private int referencedIdno;
    private Object poznamkaRozvrhare;
    private String nekonaSe;
    private String owner;
    private Object zakazaneAkce;

    @JsonProperty(defaultValue = "false")
    private boolean colliding;

    @JsonIgnore
    public boolean isColliding(Subject s) {
        var otherRect = new Rectangle2D(s.hodinaOd,0,s.hodinaDo-s.hodinaOd + 1, 1);
        var thisRect = new Rectangle2D(hodinaOd,0,hodinaDo-hodinaOd + 1, 1);

        return thisRect.intersects(otherRect);
    }
}
