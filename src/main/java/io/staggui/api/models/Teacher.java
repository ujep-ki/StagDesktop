package io.staggui.api.models;

import lombok.Data;

@Data
public class Teacher {
    private int ucitIdno;
    private String jmeno;
    private String prijmeni;
    private String titulPred;
    private String titulZa;
    private String platnost;
    private String zamestnanec;
    private int podilNaVyuce;
}
