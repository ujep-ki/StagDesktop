package io.staggui.api.models;

import lombok.Data;

import java.util.List;

@Data
public class TimeTable {
    private List<Subject> rozvrhovaAkce;
    private StudyType studyType;
}
