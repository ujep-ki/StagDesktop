package io.staggui.api.models;

import lombok.Getter;

import java.util.List;

@Getter
public class UserLoginData {
    public List<UserRole> stagUserInfo;
}
