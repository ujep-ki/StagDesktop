package io.staggui.api.models;

import lombok.Data;

@Data
public class UserRole {
    private String userName;
    private String role;
    private String roleNazev;
    private String fakulta;
    private Object katedra;
    private Object ucitIdno;
    private String aktivni;
    private boolean hasAnyRozvrharRole;
}
