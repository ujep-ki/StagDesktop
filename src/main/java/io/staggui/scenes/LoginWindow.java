package io.staggui.scenes;

import io.staggui.api.ApiClient;
import io.staggui.api.QueryParser;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import javax.swing.*;

public class LoginWindow extends Stage {
    private static final String responseUrl = "http://localhost";
    private static final String startUrl = "https://ws.ujep.cz/ws/login?originalURL=" + responseUrl;

    private static LoginWindow INSTANCE;
    private static Runnable onSuccess;

    private final WebView webView;

    private LoginWindow() {
        webView = new WebView();
        webView.getEngine().locationProperty().addListener(this::onLocationChange);

        setScene(new Scene(new BorderPane(webView)));
        setTitle("Authorization");

        initModality(Modality.WINDOW_MODAL);
    }

    private void reload(Window owner) {
        hide();
        webView.getEngine().load(startUrl);
        initOwner(owner);
    }

    public static void openAndCallOnSuccess(Window owner, Runnable onSuccess) {
        if(onSuccess == null)
            return;

        if(INSTANCE == null) INSTANCE = new LoginWindow();

        LoginWindow.onSuccess = onSuccess;

        INSTANCE.reload(owner);
        INSTANCE.show();
    }

    /**
     * Function that is called when in webview is changed 'location' property
     */
    public void onLocationChange(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if (newValue.startsWith(responseUrl)) {
            var params = QueryParser.parseUrl(newValue);

            var token = params.get("stagUserTicket");
            var data = params.get("stagUserInfo");

            ApiClient
                    .getInstance()
                    .setCredentials(token, data);

            SwingUtilities.invokeLater(onSuccess);

            close();
        }
    }
}
