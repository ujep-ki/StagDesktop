package io.staggui.scenes;

import io.staggui.api.ApiClient;
import io.staggui.api.models.Mark;
import io.staggui.api.models.Term;
import io.staggui.tools.Tools;
import io.staggui.ui.nodes.MarksHeaderItem;
import io.staggui.ui.nodes.YearItem;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import lombok.val;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

public class MarksWindow extends Stage {
    private final Consumer<String> setWindowTitle = (String something) -> setTitle("Známky (" + something + ")");

    @FXML
    private GridPane MarksTable;

    @FXML
    private ComboBox<YearItem> RCombo;

    @FXML
    private ComboBox<Term> SCombo;

    private static MarksWindow INSTANCE;

    private MarksWindow() {
        setTitle("Známky");

        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("marksWindow.fxml"));

        loader.setController(this);

        try {
            setScene(new Scene(loader.load()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void open() {
        if(INSTANCE == null) INSTANCE = new MarksWindow();

        if (!INSTANCE.isShowing()) {
           INSTANCE.show();
        }
    }

    @FXML
    void Change(ActionEvent event) throws IOException, InterruptedException {
        MakeTable();
    }

    private void MakeTable() throws IOException, InterruptedException {
        // Remove all children(cleat the gridpane)
        MarksTable.getChildren().clear();
        MarksTable.getRowConstraints().clear();

        //current studying year - selected studying year
        var selectedYearItem = RCombo.getSelectionModel().getSelectedItem();
        var selectedTerm  = SCombo.getSelectionModel().getSelectedItem();

        if(selectedTerm != null && selectedYearItem != null) {
            setWindowTitle.accept(selectedTerm.getDescription() + " " + selectedYearItem.getYear());

            var marksTask = ApiClient.getInstance().createMarksTask(selectedTerm, selectedYearItem.getYear());

            marksTask.setOnSucceeded(workerStateEvent -> {
                List<Mark> marks = marksTask.getValue();

                //header of table--------------
                String[] headNames = {"Předmět", "Hodnocení Zkoušky", "Datum", "Pokus č.:", "Zkoušející", "Hodnocení Zápočtu", "Datum", "Pokus č.:", "Zkoušející"};

                MarksTable.getChildren().clear();

                MarksTable.addRow(0);
                for (int i = 0; i < headNames.length; i++) {
                    Pane p2 = new Pane();
                    p2.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 0, 0, 1))));

                    Pane p3 = new Pane();
                    p3.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 0, 0, 1))));

                    MarksTable.add(p2, 1, 0);
                    MarksTable.add(p3, 5, 0);

                    var item = new MarksHeaderItem(headNames[i]);
                    MarksTable.add(item, i, 0);
                    GridPane.setHalignment(item, HPos.LEFT);
                }

                val borderColor = Color.rgb(222, 226, 230);
                int rowIndex = 0;

                for (Mark mark : marks) {
                    MarksTable.addRow(++rowIndex);

                    Pane p = new Pane();
                    p.setBorder(new Border(new BorderStroke(borderColor, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 0, 1, 0))));

                    MarksTable.add(p, 0, rowIndex, 9, 1);

                    List<Label> items = Tools.createMarkItems(mark);

                    Pane p2 = new Pane();
                    p2.setBorder(new Border(new BorderStroke(borderColor, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 0, 0, 1))));

                    Pane p3 = new Pane();
                    p3.setBorder(new Border(new BorderStroke(borderColor, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 0, 0, 1))));

                    MarksTable.add(p2, 1, rowIndex);
                    MarksTable.add(p3, 5, rowIndex);

                    for (int i = 0; i < items.size(); i++) {
                        MarksTable.add(items.get(i), i, rowIndex);
                        GridPane.setMargin(items.get(i), new Insets(5));
                    }

                    GridPane.setHalignment(items.get(1), HPos.CENTER);
                    GridPane.setHalignment(items.get(3), HPos.CENTER);
                    GridPane.setHalignment(items.get(5), HPos.CENTER);
                    GridPane.setHalignment(items.get(7), HPos.CENTER);
                }
            });

            new Thread(marksTask).start();
        }
    }

    @FXML
    public void initialize() {
        RCombo.setItems(Tools.generateYears());
        SCombo.setItems(FXCollections.observableArrayList(Term.values()));

        var currentAcademicDataTask = ApiClient
            .getInstance()
            .createCurrentAcademicInfoTask();

        currentAcademicDataTask.setOnSucceeded(workerStateEvent ->
            currentAcademicDataTask.getValue().ifPresent(currentAcademicStateInfo -> {
                SCombo.getSelectionModel().select(currentAcademicStateInfo.getObdobi());

                for (YearItem item : RCombo.getItems()) {
                    if(item.getYear() == Integer.parseInt(currentAcademicStateInfo.getAkademRok())) {
                        RCombo.getSelectionModel().select(item);
                    }
                }

                try {
                    MakeTable();
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            })
        );
    }
}
