package io.staggui.ui.nodes;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class MarksHeaderItem extends BorderPane {
    public MarksHeaderItem(String headName) {
        var header = new Label(headName);

        setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 0, 1, 0))));
        setCenter(header);

        BorderPane.setMargin(header, new Insets(5));
    }
}
