package io.staggui.ui.nodes;

import io.staggui.api.models.Subject;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.TextAlignment;
import lombok.Getter;

@Getter
public class TimeTableItem extends BorderPane {
    private static final Background lectureBackground = createBackgroundFromPaint(Color.rgb(227, 227, 227));
    private static final Background seminarBackground = createBackgroundFromPaint(Color.rgb(164, 239, 151));
    private static final Background examBackground = createBackgroundFromPaint(Color.rgb(158, 229, 193));

    private static final Border b = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderStroke.THIN));

    private final Subject subject;

    public TimeTableItem(Subject subject) {
        this.subject = subject;
        
        BorderPane root = new BorderPane();

        defineMainDetails(root, subject);
        defineTeachers(root, subject);
        setBackgroundByType(root, subject.getTypAkceZkr());

        setCenter(root);

        root.setOnMouseEntered(mouseEvent -> setBorder(true));
        root.setOnMouseExited(mouseEvent -> setBorder(false));

        BorderPane.setMargin(root, new Insets(2));
    }

    private void defineMainDetails(BorderPane root, Subject subject) {
        VBox mainInfoBox = new VBox();
        Label subName = new Label(subject.getKatedra()+"/"+ subject.getPredmet());
        Label room = new Label("Room: " + subject.getBudova() + subject.getMistnost());
        mainInfoBox.setAlignment(Pos.CENTER);

        subName.setTextAlignment(TextAlignment.CENTER);
        room.setTextAlignment(TextAlignment.CENTER);

        mainInfoBox.getChildren().addAll(subName, room);

        root.setTop(mainInfoBox);
    }

    private void defineTeachers(BorderPane root, Subject subject) {
        VBox teachersBox = new VBox();
        teachersBox.setAlignment(Pos.CENTER);

        String[] teachers = subject
            .getVsichniUcitelePrijmeni()
            .split("[,]");

        for (String s : teachers) {
            Label teacher = new Label(s);
            teacher.setAlignment(Pos.CENTER);

            teachersBox.getChildren().add(teacher);
        }

        root.setCenter(teachersBox);
    }

    private void setBorder(boolean enable) {
        if(enable) {
            setBorder(b);
        }else{
            setBorder(null);
        }
    }

    private void setBackgroundByType(Pane node, String type) {
        if(type.equalsIgnoreCase("př")) {
            node.setBackground(lectureBackground);
        }else if(type.equalsIgnoreCase("se")) {
            node.setBackground(seminarBackground);
        }else if(type.equalsIgnoreCase("cv")) {
            node.setBackground(examBackground);
        }
    }

    private static Background createBackgroundFromPaint(Paint paint) {
        return new Background(new BackgroundFill(paint, new CornerRadii(5), Insets.EMPTY));
    }

    public void addToTimeTable(GridPane gp) {
        int rowLocation = (subject.getDenZkr().ordinal() * 2) + (subject.isColliding() ? 2 : 1);
        int colSpan = subject.getHodinaDo() - subject.getHodinaOd() + 1;

        Platform.runLater(() -> gp.add(this, subject.getHodinaOd(), rowLocation, colSpan, 1));
    }
}
